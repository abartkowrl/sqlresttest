using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.SqlClient;
using Dapper;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SQLRest.SQL
{
    public class DataAccess
    {
        private const string connString = "Data Source=localhost\\sql2016;Initial Catalog=luceco_live;" + "User ID=sa;" + "Password=f00tbaLL";

        private static Dictionary<Type, DbType> typeMap = new Dictionary<Type, DbType>
        {
            {typeof(byte), DbType.Byte},
            {typeof(sbyte), DbType.SByte},
            {typeof(short), DbType.Int16},
            {typeof(ushort), DbType.UInt16},
            {typeof(int), DbType.Int32},
            {typeof(uint), DbType.UInt32},
            {typeof(long), DbType.Int64},
            {typeof(ulong), DbType.UInt64},
            {typeof(float), DbType.Single},
            {typeof(double), DbType.Double},
            {typeof(decimal), DbType.Decimal},
            {typeof(bool), DbType.Boolean},
            {typeof(string), DbType.String},
            {typeof(char), DbType.StringFixedLength},
            {typeof(Guid), DbType.Guid},
            {typeof(DateTime), DbType.DateTime},
            {typeof(DateTimeOffset), DbType.DateTimeOffset},
            {typeof(byte[]), DbType.Binary},
            {typeof(byte?), DbType.Byte},
            {typeof(sbyte?), DbType.SByte},
            {typeof(short?), DbType.Int16},
            {typeof(ushort?), DbType.UInt16},
            {typeof(int?), DbType.Int32},
            {typeof(uint?), DbType.UInt32},
            {typeof(long?), DbType.Int64},
            {typeof(ulong?), DbType.UInt64},
            {typeof(float?), DbType.Single},
            {typeof(double?), DbType.Double},
            {typeof(decimal?), DbType.Decimal},
            {typeof(bool?), DbType.Boolean},
            {typeof(char?), DbType.StringFixedLength},
            {typeof(Guid?), DbType.Guid},
            {typeof(DateTime?), DbType.DateTime},
            {typeof(DateTimeOffset?), DbType.DateTimeOffset},
            //{typeof(System.Data.Linq.Binary), DbType.Binary}
        };
        public List<object> Get(string query)
        {
            return ReadOrderData(connString,query);
        }

        public bool DoCommand(string query, List<RestParam> restParams, out List<RestParam> outParams)
        {
            return ExecuteNonQuery(query,connString, restParams, out outParams);
        }
        
        private static bool ExecuteNonQuery(string queryString, string connectionString, List<RestParam> restParams, out List<RestParam> outParams)
        {
            outParams = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(
                connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    var parameters = new DynamicParameters();
                    
                    foreach (var rParam in restParams)
                    {
                        var v = JsonConvert.DeserializeObject(rParam.Value, Type.GetType(rParam.ValueType));
                        
                        if (rParam.Direction == "Output")
                        {
                            //command.Parameters.Add(new SqlParameter(rParam.Name,v));
                            //command.Parameters.Add(rParam.Name,typeMap[v.GetType()]);
                            //command.Parameters[rParam.Name].Value = v;
                            //command.Parameters[rParam.Name].Direction = ParameterDirection.Output;
                            parameters.Add(rParam.Name,v,typeMap[v.GetType()],ParameterDirection.Output);
                        }
                        else
                        {
                            //command.Parameters.Add(rParam.Name,typeMap[v.GetType()]);
                            //command.Parameters[rParam.Name].Value = v;
                            parameters.Add(rParam.Name,v,typeMap[v.GetType()],ParameterDirection.Input);
                            //command.Parameters.Add(new SqlParameter(rParam.Name,v));
                        }
                    }
                    
                    //parameters.Add("@SuccessOUT",true,DbType.Boolean,ParameterDirection.Output);

                    try
                    {
                        connection.Open();
                        //var rows = command.ExecuteNonQuery();
                        var rows = connection.Execute(queryString, parameters, commandType: CommandType.StoredProcedure);
                        if (rows > 0)
                        {
                        
                        }
                        //connection.Open();
                        //var results = connection.Query(queryString, dP, null, true, null, CommandType.StoredProcedure);
                        restParams.RemoveAll(p => p.Direction == "Input");
                        foreach (var rParam in restParams)
                        {
                            var v = parameters.Get<dynamic>(rParam.Name);
                            rParam.Value = JsonConvert.SerializeObject(v);
                        }
                        connection.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }

                }
                outParams = restParams;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                outParams = null;
                return false;
            }
        }
        
        private static List<object> ReadOrderData(string connectionString, string query)
        {
            
            using (SqlConnection connection = new SqlConnection(
                connectionString))
            {
                try
                {
                    connection.Open();
                    var items = connection.Query(query).ToList();
                    connection.Close();
                    return items;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return null;
                }
            }
        }
    }
    
    public class RestParam
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string ValueType { get; set; }
        public string Direction { get; set; }
    }
}