﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SQLRest.SQL;

namespace SQLRest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DbAccessController : ControllerBase
    {
        private readonly ILogger<DbAccessController> _logger;

        public DbAccessController(ILogger<DbAccessController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get(string query)
        {
            if (string.IsNullOrEmpty(query)) return BadRequest("nope!");
            var d = new DataAccess();
            
            return Ok(d.Get(query));
        }

        [HttpPost]
        public IActionResult Post(string query,[FromBody] List<RestParam> restParams)
        {
            if (string.IsNullOrEmpty(query)) return BadRequest("nope!");
            var d = new DataAccess();
            d.DoCommand(query,restParams, out var output);
            return Ok(output);
        }
    }
}